from bs4 import BeautifulSoup
import requests
import json
import os

BASE_URL = 'https://arxiv.org'
URL = '%s/list/cs/pastweek?show=500' % (BASE_URL)
FOLDER = './arxiv_cs_test/'

doc = requests.get(URL)
soup = BeautifulSoup(doc.text, 'html.parser')


KEY_FILE = 'testset_paper_keys.json'
papers = []

# make the folder for storing the papers
if not os.path.isdir(FOLDER):
	os.mkdir("%s" % (FOLDER))


# Loop over all pdf links and extract file & save metadata to keys
for a in soup.find_all('a', string='pdf'):

	# Find metadata
	dt = a.find_parent('dt')
	dd = dt.next_sibling.next_sibling

	title = dd.find('div', {"class": "list-title"})
	subjects = dd.find('div', {"class": "list-subjects"})

	# Create Key of papers metadata
	x = {
		'file': '%s%s.pdf' % (FOLDER, title.text.split(': ')[1].strip('\n').replace('/', '|')),
		'url': '%s%s' % (BASE_URL, a.get('href')),
		'title': title.text.split(': ')[1],
		'subjects': [sub.replace('\n', '') for sub in subjects.text.split(': ')[1].split('; ')]
	}
	print(x['url'])

	# Append keys
	papers.append(x)

	# Grab & Save file
	paper = requests.get(x['url'])

	if paper.status_code == 200:
		open("%s" % (x['file']), 'wb').write(paper.content)

with open(KEY_FILE, mode='w') as f:
	f.write(json.dumps(papers, indent=4))
	f.close()