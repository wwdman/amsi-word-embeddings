from gensim.models import Word2Vec
import numpy

# size: the dimensionality of the embedding vectors.
# window: the maximum distance between the current and predicted word within a sentence.


sentences = [["this", "is", "a", "test", "sentance"], ["i", "like", "pizza", "very", "much"], ["this", "sentance", "is", "not", "similar"]]
model = Word2Vec(sentences, size=8, window=2, min_count=1)

vecs = []

for sentance in sentences:

	total = numpy.array(model.wv[sentance[0]])
	for idx, token in enumerate(sentance):

		if idx != 0:
			total += numpy.array(model.wv[token])

	print(total)

	vecs.append(total)

print(vecs)