pred = [
    [
        19,
        "Information Theory (cs.IT)",
        11.979852198095235
    ],
    [
        7,
        "Signal Processing (eess.SP)",
        4.366962828440488
    ],
    [
        2,
        "Combinatorics (math.CO)",
        1.2747477813218662
    ],
    [
        2,
        "Systems and Control (eess.SY)",
        1.201827248559641
    ],
    [
        1,
        "Algebraic Geometry (math.AG)",
        0.6118970929406469
    ],
    [
        1,
        "Social and Information Networks (cs.SI)",
        0.6093900186095699
    ],
    [
        1,
        "Machine Learning (stat.ML)",
        0.6021466269984709
    ],
    [
        1,
        "Machine Learning (cs.LG)",
        0.6021466269984709
    ],
    [
        1,
        "Optimization and Control (math.OC)",
        0.6016386856714999
    ],
    [
        1,
        "Networking and Internet Architecture (cs.NI)",
        0.588079165084111
    ],
    [
        1,
        "Data Structures and Algorithms (cs.DS)",
        0.5813382787425699
    ]
] 


x = [p[1] for p in pred if (p[2]/p[0]) > 0.62]
print(x)