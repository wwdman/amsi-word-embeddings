import textract
import json
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords



READ_KEY_FILE = 'testset_paper_keys.json'
KEY_TERM_FILE = 'testset_paper_key_terms.json'


def getKeyTerms(filename):
	text = ""

	try:
		text = textract.process(filename, method='pdfminer', language='eng', encoding='ascii')

		#clean text
		text = text.decode("utf-8")
		text = text.replace('\n\n', ' ')
		text = text.replace('\n', ' ')
		text = text.lower()
	except:
		print("ascii didnt work, trying UTF-8")
		return ["ascii didnt work"]


	#The word_tokenize() function will break our text phrases into #individual words
	tokens = word_tokenize(text)

	#we'll create a new list which contains punctuation we wish to clean
	punctuations = ['(',')',';',':','[',']',',', '{', '}', '.']

	#We initialize the stopwords variable which is a list of words like 
	#"The", "I", "and", etc. that don't hold much value as keywords
	stop_words = stopwords.words('english')

	#We create a list comprehension which only returns a list of words 
	#that are NOT IN stop_words and NOT IN punctuations.
	keywords = [word for word in tokens if not word in stop_words and not word in punctuations]

	return keywords


'''
The main program loops over all the files stored in the READ_KEY_FILE, the key 
terms are then extracted and inserted into a new file KEY_TERM_FILE
'''
keyFile = json.load(open(READ_KEY_FILE, 'r'))
i = 0

for file in keyFile:
	# To follow along on progress in the CLI
	print("%s: %s" % (i, file["file"]))

	# Extract terms
	file["terms"] = getKeyTerms(file["file"])
	i +=1


# Write the terms to a json file
with open(KEY_TERM_FILE, mode='w') as f:
	f.write(json.dumps(keyFile, indent=4))
	f.close()