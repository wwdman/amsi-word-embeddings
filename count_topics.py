import json

READ_KEY_FILE = 'paper_keys.json'

keyFile = json.load(open(READ_KEY_FILE, 'r'))
topics = []

for file in keyFile:

	for topic in file["subjects"]:

		topic = topic.replace('\n', '')

		if topic not in topics:
			topics.append(topic)


print(len(topics))
print(topics)
