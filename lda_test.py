'''
This program uses LDA to cluster papers together based on the topics extracted
Based on:
https://github.com/priya-dwivedi/Deep-Learning/blob/master/topic_modeling/LDA_Newsgroup.ipynb

Words that have fewer than 3 characters are removed.
All stopwords are removed.
Words are lemmatized — words in third person are changed to first person and verbs in past and future tenses are changed into present.
Words are stemmed — words are reduced to their root form.
'''
import json
import gensim
from gensim.utils import simple_preprocess
from gensim.parsing.preprocessing import STOPWORDS
from nltk.stem import WordNetLemmatizer, SnowballStemmer
from nltk.stem.porter import *
import numpy as np

import pyLDAvis
import pyLDAvis.gensim

np.random.seed(400)
import nltk


TOPIC_FILE = 'paper_topic_terms.json'
TOPC_SAVE = 'corpus_topics.json'
STEMMER = SnowballStemmer("english")

processed_docs = json.load(open(TOPIC_FILE, 'r'))
dictionary = gensim.corpora.Dictionary(processed_docs)

print(dictionary)

dictionary.filter_extremes(no_below=15, no_above=0.1, keep_n= 100000)

print(dictionary)

bow_corpus = [dictionary.doc2bow(doc) for doc in processed_docs]


document_num = 20
bow_doc_x = bow_corpus[document_num]

for i in range(len(bow_doc_x)):
    print("Word {} (\"{}\") appears {} time.".format(bow_doc_x[i][0], 
                                                     dictionary[bow_doc_x[i][0]], 
                                                     bow_doc_x[i][1]))


num_of_topics = 102

lda_model =  gensim.models.LdaMulticore(bow_corpus, 
                                   num_topics = num_of_topics, 
                                   id2word = dictionary,                                    
                                   passes = 2,
                                   alpha = 1/(num_of_topics/2),
                                   eta = 1/(num_of_topics/2))


for idx, topic in lda_model.print_topics(-1):
    print("Topic: {} \nWords: {}".format(idx, topic ))
    print("\n")


print("lda model of doc 20")
print(lda_model[bow_doc_x])

vis = pyLDAvis.gensim.prepare(topic_model=lda_model, corpus=bow_corpus, dictionary=dictionary)
#pyLDAvis.enable_notebook()
pyLDAvis.display(vis)

'''
Topic: 0
Words: 0.007*"sentenc" + 0.005*"reward" + 0.004*"lstm" + 0.004*"music" + 0.004*"token" + 0.004*"caption" + 0.004*"driver" + 0.004*"workow" + 0.004*"trafc" + 0.004*"remot"


Topic: 1
Words: 0.016*"quantum" + 0.015*"vertex" + 0.008*"subgraph" + 0.006*"dictionari" + 0.006*"walk" + 0.006*"predic" + 0.006*"citat" + 0.005*"worker" + 0.005*"corpus" + 0.005*"cliqu"


Topic: 2
Words: 0.024*"cid:62" + 0.011*"reward" + 0.010*"privaci" + 0.009*"rout" + 0.006*"client" + 0.006*"persist" + 0.005*"player" + 0.005*"trac" + 0.005*"manifold" + 0.005*"privat"


Topic: 3
Words: 0.022*"cid:126" + 0.010*"cid:27" + 0.007*"cid:5" + 0.007*"dialogu" + 0.006*"cancer" + 0.006*"contact" + 0.005*"wave" + 0.005*"galerkin" + 0.005*"cid:29" + 0.004*"cid:101"


Topic: 4
Words: 0.065*"0.00" + 0.026*"cid:31" + 0.014*"neuron" + 0.009*"1.00" + 0.009*"plant" + 0.008*"anomali" + 0.007*"0.03" + 0.007*"dnns" + 0.006*"0.04" + 0.006*"interfer"


Topic: 5
Words: 0.035*"cid:98" + 0.021*"cid:101" + 0.015*"cid:63" + 0.010*"wireless" + 0.009*"posterior" + 0.009*"quantize" + 0.008*"cid:74" + 0.007*"textur" + 0.006*"transmit" + 0.006*"cid:44"


Topic: 6
Words: 0.015*"blockchain" + 0.012*"consent" + 0.010*"fault" + 0.010*"rain" + 0.009*"websit" + 0.009*"layout" + 0.009*"hash" + 0.008*"contract" + 0.008*"belief" + 0.007*"checkpoint"


Topic: 7
Words: 0.004*"sentenc" + 0.004*"prune" + 0.003*"salienc" + 0.003*"deform" + 0.003*"meta-learn" + 0.003*"linguist" + 0.003*"few-shot" + 0.003*"ne-tun" + 0.003*"backbon" + 0.003*"distil"
'''

'''
count = 0
for k, v in dictionary.iteritems():
    print(k, v)
    count += 1
    if count > 10:
        break

''
# Write the terms to a json file
with open(TOPC_SAVE, mode='w') as f:
    f.write(json.dumps(processed_docs, indent=4))
    f.close()
'''