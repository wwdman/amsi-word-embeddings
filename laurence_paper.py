import json
import os

BASE_URL = 'https://arxiv.org'
URL = '%s/list/cs/pastweek?show=500' % (BASE_URL)
FOLDER = './laurence_papers/'


KEY_FILE = 'laurence_paper_keys.json'
papers = []

onlyfiles = [f for f in os.listdir(FOLDER) if os.path.isfile(os.path.join(FOLDER, f))]

print(json.dumps(onlyfiles, indent=4))

for file in onlyfiles:
	x = {
		'file': "%s%s" % (FOLDER, file.replace('/', '|')),
		'url': file,
		'title': file.replace('/', '|'),
		'subjects': []
	}
	print(x['url'])

	# Append keys
	papers.append(x)


with open(KEY_FILE, mode='w') as f:
	f.write(json.dumps(papers, indent=4))
	f.close()

'''
# make the folder for storing the papers
if not os.path.isdir(FOLDER):
	os.mkdir("%s" % (FOLDER))


# Loop over all pdf links and extract file & save metadata to keys
for a in soup.find_all('a', string='pdf'):

	# Find metadata
	dt = a.find_parent('dt')
	dd = dt.next_sibling.next_sibling

	title = dd.find('div', {"class": "list-title"})
	subjects = dd.find('div', {"class": "list-subjects"})

	# Create Key of papers metadata
	
'''