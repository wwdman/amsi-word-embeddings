'''
This program uses LDA to cluster papers together based on the topics extracted
Based on:
https://github.com/priya-dwivedi/Deep-Learning/blob/master/topic_modeling/LDA_Newsgroup.ipynb

Words that have fewer than 3 characters are removed.
All stopwords are removed.
Words are lemmatized — words in third person are changed to first person and verbs in past and future tenses are changed into present.
Words are stemmed — words are reduced to their root form.
'''
import json
import gensim
from gensim.utils import simple_preprocess
from gensim.parsing.preprocessing import STOPWORDS
from nltk.stem import WordNetLemmatizer, SnowballStemmer
from nltk.stem.porter import *
import numpy as np
np.random.seed(400)
import nltk


KEY_TERM_FILE = 'testset_paper_key_terms.json'
TOPIC_FILE = 'testset_paper_topic_terms_2.json'
STEMMER = SnowballStemmer("english")


def lemmatize_stemming(text):
    return WordNetLemmatizer().lemmatize(text, pos='v')


# Tokenize and lemmatize
def preprocess(text):
    result=[]
    for token in text:
        if token not in gensim.parsing.preprocessing.STOPWORDS and len(token) > 3 and ':' not in token and not token.replace('.', '', 1).isdigit():
            result.append(lemmatize_stemming(token))
            
    return result



keyFile = json.load(open(KEY_TERM_FILE, 'r'))
processed_docs = []
i = 0

for doc in keyFile:
	print("%s: %s" % (i, doc["file"]))
	processed_docs.append(preprocess(doc["terms"]))
	i += 1

# Write the terms to a json file
with open(TOPIC_FILE, mode='w') as f:
	f.write(json.dumps(processed_docs, indent=4))
	f.close()